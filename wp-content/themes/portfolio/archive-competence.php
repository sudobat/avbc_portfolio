<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package portfolio
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title"><?php post_type_archive_title(); ?></h1>
				<?php
					# the_archive_title( '<h1 class="page-title">', '</h1>' );
					# the_archive_description( '<div class="taxonomy-description">', '</div>' );
				?>
			</header><!-- .page-header -->

			<div class="container-fluid">

			<?php /* Start the Loop */ ?>
			<?php
			/**
			 * I introduce a counter to adapt the
			 * WordPress Loop with BootStrap Grid
			 */
			$i = 1;
			?>
			<?php while ( have_posts() ) : the_post(); ?>

			<?php 	if ($i % 4 == 1): # We have to open a row & col-md & row ?>
				<div class="row">
					<div class="col-md-6">
						<div class="row">
			<?php 	elseif ($i % 4 == 3): # We have to open a col-md & row ?>
					<div class="col-md-6">
						<div class="row">
			<?php 	endif; ?>

					<div class="col-sm-6">

				<?php
					/* Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'content', 'competence' );
				?>

					</div><!-- .col-sm-6 -->

			<?php 	if ($i % 4 == 0): # We have to close a row & col-md & row ?>
						</div><!-- .row -->
					</div><!-- .col-md-6 -->
				</div><!-- .row -->
			<?php 	elseif ($i % 4 == 2): # We have to close a col-md & row ?>
					</div><!-- .col-md-6 -->
				</div><!-- .row -->
			<?php 	endif; ?>

			<?php 	$i++; ?>

			<?php endwhile; ?>

			<?php $i--; # Delete the last increment on $i to know where it finished ?>
			<?php if ($i % 4 != 0): # Extra Check to see if we have to close the last row ?>
			<?php 	if (($i % 4 == 1) or ($i % 4 == 3)): # Check if we have to close inner row & col-md too ?>
						</div><!-- .row -->
					</div><!-- .col-md-6 -->
			<?php 	endif; ?>
				</div><!-- .row -->
			<?php endif; ?>
			
			</div><!-- .container-fluid -->

			<?php the_posts_navigation(); ?>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php # get_sidebar(); ?>
<?php get_footer(); ?>
