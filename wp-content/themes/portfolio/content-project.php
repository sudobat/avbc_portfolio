<?php
/**
 * The template part for displaying results in search pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package portfolio
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		
		<div class="cycle-slideshow"
			data-cycle-slides=".cycle-slide"
		>
		<?php
		$images = get_custom_field('image:to_image_src');

		if ( ! empty($images)) {
			foreach ($images as $image) {
				printf('<div class="cycle-slide" style="background-image:url(\'%s\');"></div>', $image);
			}
		}
		?>
		</div><!-- .cycle-slideshow -->

		<?php the_title( sprintf( '<h1 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' ); ?>

		<strong>Url</strong> <?php print_custom_field('url'); ?><br />
		<strong>Customer</strong> <?php print_custom_field('customer'); ?>

		<?php if ( 'post' == get_post_type() ) : ?>
		<div class="entry-meta">
			<?php portfolio_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->

	<footer class="entry-footer">
		<?php portfolio_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
