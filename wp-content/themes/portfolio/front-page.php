<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package portfolio
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php /* Here we load our presentation card (FrontPage Content) */ ?>
		<?php if ( have_posts() ) : ?>

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php
					/* Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'content', 'home' );
				?>

			<?php endwhile; ?>

			<?php the_posts_navigation(); ?>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>


		<?php /* Here we load our 3 last projects using a custom Wordpress Query */

		$projects_query = new WP_Query([
			'post_type'			=> 'project',
			'posts_per_page'	=> 3,
			'orderby'			=> 'date',
			'order'				=> 'DESC'
		]);
		
		?>

		<?php if ( $projects_query->have_posts() ) : ?>

			<h2 class="block-title">My Last Projects</h2>

			<div class="container-fluid">
				<div class="row">

			<?php /* Start the Loop */ ?>
			<?php while( $projects_query->have_posts() ) : $projects_query->the_post(); ?>

					<div class="col-sm-4">

				<?php get_template_part('content', 'project'); ?>

					</div><!-- .col-sm-4 -->

			<?php endwhile; ?>

				</div><!-- .row -->
			</div><!-- .container-fluid -->

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>


		<?php /* Here we load our competences using a custom Wordpress Query */

		$competences_query = new WP_Query([
			'post_type'			=> 'competence',
			'posts_per_page'	=> -1,
			'meta_key'			=> 'level',
			'orderby'			=> 'meta_value_num',
			'order'				=> 'DESC'
		]);
		
		?>

		<?php if ( $competences_query->have_posts() ) : ?>

			<h2 class="block-title">My Competences</h2>

			<div class="cycle-slideshow" 
				data-cycle-slides=".competence-slide" 
				data-cycle-fx=carousel 
				data-cycle-carousel-visible=2 
				data-cycle-carousel-fluid=true 
			>

			<?php /* Start the Loop */ ?>
			<?php while( $competences_query->have_posts() ) : $competences_query->the_post(); ?>

					<div class="competence-slide">

				<?php get_template_part('content', 'competence'); ?>

					</div><!-- .competence-slide -->

			<?php endwhile; ?>

			</div><!-- .cycle-slideshow -->

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php # get_sidebar(); ?>
<?php get_footer(); ?>
