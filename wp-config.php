<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'portfolio');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'xF>)#G^!|0R)+k|Q%EjIsIF^6)+V1-y=`.-i|}Q#S,H,;;QR&y%-X-.W`npK#*zd');
define('SECURE_AUTH_KEY',  'jr}Fq9@@p}lKswAJS.lr&pt*6scl8R-Ks8l-VuudESs n|@ -UV+f@Ku0u7[[HyS');
define('LOGGED_IN_KEY',    '%YCPN>?]AsT9Hh6nW;Sb#xjYvq4$UV;15mZFw60Ea2:^$f+7ad=,y8EZs2h7r7k_');
define('NONCE_KEY',        'BD{|^Nu7F3FJY,LX4fbB4Z)waxvkbJ:ng^||G_i(M2+e(*pqS|KYlw:e4~tPB:@5');
define('AUTH_SALT',        'vF&i0~@$N$?t%T&@wowdYB!HV):$70]k(K&HJ7p,+6Z:yf#Zy@4-ML)$^ikZ+Loy');
define('SECURE_AUTH_SALT', '|GPLByY?@fh:~yO>1.oTxK!V#.h$5u0Ir|^9 -m8E3#*If_M!gxuP1?@ar>2LUE[');
define('LOGGED_IN_SALT',   '_7*TQZK+4$2@p|&I@35qRT4W3NTL:]&ps7CD!vC>zQhA-7?H%vuL~hx/J`D4qj5J');
define('NONCE_SALT',       '_pJj!yiLtR-[J}~=6)mL5sc?A_+iR.sPguP54X^Kyz|E-{^BKt+[9JGJM,e0wgda');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'portfolio_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
